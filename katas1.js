console.log("Retornar os números de 1 a 20. (1, 2, 3,…, 19, 20)")
function UmAteVinte() {
    let Resultado=[]
    for(let contagem =1; contagem <= 20; contagem++) {
        Resultado += contagem + ","
    }
    return Resultado
    }
    console.log(UmAteVinte());
console.log("Retornar os números pares de 1 a 20. (2, 4, 6,…, 18, 20)")
function UmAteVintePar() {
    let Resultado=[]
    for(let contagem =0; contagem <= 20; contagem += 2) {
    Resultado += contagem + ","
}
return Resultado
}
console.log(UmAteVintePar());
console.log("Retornar os números ímpares de 1 a 20. (1, 3, 5,…, 17, 19)")
function UmAteVinteImpar() {
    let Resultado=[]
    let contagem = 1
    for(contagem =1; contagem <= 20; contagem += 2) {
        Resultado += contagem + ","
    }
    return Resultado
    }
    console.log(UmAteVinteImpar());
console.log("Retornar os múltiplos de 5 até 100. (5, 10, 15,…, 95, 100)")
function DeCincoEmCincoAteCem() {
    let Resultado=[]
    let contagem = 5
    for(contagem =5; contagem <= 100; contagem += 5){
        Resultado += contagem + ","
    }
    return Resultado
    }
    console.log(DeCincoEmCincoAteCem());
console.log("Retornar todos os números até 100 que forem quadrados perfeitos. (1, 4, 9, …, 81, 100)")
function QuadradoPerfeitoSoma() {  
    let Resultado=[]
    for(let contagem =1; contagem <= 10; contagem++){
        Resultado += contagem*contagem + ","
    }
    return Resultado
    }
    console.log(QuadradoPerfeitoSoma());
console.log("Retornar os números contando de trás para frente de 20 até 1. (20, 19, 18, …, 2, 1)")
function VinteAteUm() { 
    let Resultado=[] 
    for(let contagem =20; contagem >= 1; contagem -= 1 ){
        Resultado += contagem + ","
    }
    return Resultado
    }
    console.log(VinteAteUm());
console.log("Retornar os números pares de 20 até 1. (20, 18, 16, …, 4, 2)")
function VinteAteUmPar() {  
    let Resultado=[]
    for(let contagem =20; contagem >= 1; contagem -= 2 ){
        Resultado += contagem + ","
    }
    return Resultado
    }
    console.log(VinteAteUmPar());
console.log("Retornar os números ímpares de 20 até 1. (19, 17, 15, …, 3, 1)")
function VinteAteUmImpar() { 
    let Resultado=[] 
    for(let contagem =19; contagem >= 1; contagem -= 2 ){
        Resultado += contagem + ","
    }
    return Resultado
    }
    console.log(VinteAteUmImpar());
console.log("Retornar os múltiplos de 5 contando de trás para frente a partir de 100. (100, 95, 90, …, 10, 5)")
function DeCincoEmCincoAteUm() {  
    let Resultado=[]
    for(let contagem =100; contagem >= 1; contagem -= 5 ){
        Resultado += contagem + ","
    }
    return Resultado
    }
    console.log(DeCincoEmCincoAteUm());
console.log("Retornar os quadrados perfeitos contando de trás para frente a partir de 100. (100, 81, 64, …, 4, 1)")
function QuadradoPerfeitoSub() {  
    let Resultado=[]
    for(let contagem =10; contagem >= 1; contagem -= 1){
        Resultado += contagem * contagem + ","
    }
    return Resultado
    }
    console.log(QuadradoPerfeitoSub());
